LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),RMX2170)

RADIO_FILES := $(wildcard $(LOCAL_PATH)/radio/*)
$(foreach f, $(notdir $(RADIO_FILES)), \
    $(call add-radio-file,radio/$(f)))

FIRMWARE_IMAGES := \
abl \
aop \
BTFM \
cdt_engineering \
cmnlib \
cmnlib64 \
devcfg \
dpAP \
DRIVER \
dspso \
hyp \
imagefv \
keymaster64 \
modem \
oppo_sec \
qupv3fw \
splash \
static_nvbk \
storsec \
tz \
uefisecapp \
xbl \
xbl_config

endif
